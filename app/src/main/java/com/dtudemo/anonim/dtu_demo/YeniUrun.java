package com.dtudemo.anonim.dtu_demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.Result;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class YeniUrun extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener
{
    private static final String TAG = YeniUrun.class.getSimpleName();
    Button btnAnasayfa, btnBarkod;
    General gnlr;
    TextView lblBarcode;

    //private ZXingScannerView zXingScannerView;
    private BarcodeReader barcodeReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yeni_urun);
        btnAnasayfa = findViewById(R.id.btnAnasayfa);
        //btnBarkod = findViewById(R.id.btnBarcode);
        //lblBarcode = findViewById(R.id.lblBarcode);
        gnlr = new General();
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);

    }



    public void anasayfaGit(View view)
    {

    }

    @Override
    protected void onPause(){
        super.onPause();
        //zXingScannerView.stopCamera();
    }


    @Override
    public void onScanned(final Barcode barcode) {
        Log.e(TAG, "onScanned: " + barcode.displayValue);
        barcodeReader.playBeep();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Barkod Okundu!: " + barcode.displayValue, Toast.LENGTH_SHORT).show();
                gnlr.setBarcodeResult(barcode.displayValue);
            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
      /*  String codes = "";
        for (Barcode barcode : barcodes) {
            codes += barcode.displayValue + ", ";
        }

        final String finalCodes = codes;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "BARKOD: " + finalCodes, Toast.LENGTH_SHORT).show();
                lblBarcode.setText(finalCodes);
            }
        });*/
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {
    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }
}
