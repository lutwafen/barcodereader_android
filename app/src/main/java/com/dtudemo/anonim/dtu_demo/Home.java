package com.dtudemo.anonim.dtu_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home extends AppCompatActivity {

    Button btnTumUrunler;
    Button btnYeniUrun;

    General gnlr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        gnlr = new General();

        btnTumUrunler = findViewById(R.id.btnTumUrunler);
        btnYeniUrun = findViewById(R.id.btnUrunEkle);

    }

    public void YeniUrunClick(View view)
    {
        Intent yeniUrun = new Intent(Home.this, YeniUrun.class);
        startActivity(yeniUrun);
    }
    public void TumUrunlerClick(View view)
    {
    }


}
